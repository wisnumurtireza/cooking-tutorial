package wisnumurti.reza.cookingtutorials

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_memasak.*
import kotlinx.android.synthetic.main.activity_jadwal.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ActivityJadwal : AppCompatActivity(), View.OnClickListener{
    lateinit var mediaHelper: MediaHelper
    lateinit var jadwalAdapter: AdapterJadwal
    lateinit var genreAdapter: ArrayAdapter<String>
    var daftarJadwal = mutableListOf<HashMap<String,String>>()
    var daftarGenre = mutableListOf<String>()
    val url4 = "http://192.168.43.21/memasak/show_jadwal.php"
    var url2 = "http://192.168.43.21/memasak/show_genre.php"
    var url5 = "http://192.168.43.21/memasak/query_ins_up_del_jadwal.php"
    var imStr = ""
    var pilihGenre = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jadwal)
        jadwalAdapter = AdapterJadwal(daftarJadwal,this)
        mediaHelper = MediaHelper(this)
        lsjadwal.layoutManager = LinearLayoutManager(this)
        lsjadwal.adapter = jadwalAdapter

        genreAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarGenre)
        sp_gendr.adapter = genreAdapter
        sp_gendr.onItemSelectedListener = itemSelected

        imgUpload.setOnClickListener(this)
        btnIns.setOnClickListener(this)
        btnUpd.setOnClickListener(this)
        btnHps.setOnClickListener(this)
        btn_find.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUpload -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }R.id.btnIns -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btnHps -> {
                queryInsertUpdateDelete("delete")
            }
            R.id.btnUpd -> {
                queryInsertUpdateDelete("update")
            }
            R.id.btn_find -> {
                showDataJadwal(ed_jdl.text.toString().trim())
            }
        }
    }
    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spgenree.setSelection(0)
            pilihGenre = daftarGenre.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihGenre = daftarGenre.get(position)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == mediaHelper.getRcGallery()) {
                imStr = mediaHelper.getBitmapToString(data!!.data, imgUpload)
            }
        }
    }

    fun queryInsertUpdateDelete(mode : String) {
        val request = object : StringRequest(
            Method.POST, url5,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")) {
                    Toast.makeText(this, "Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataJadwal("")
                } else {
                    Toast.makeText(this, "Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }) {

            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                val nmFile = "DC" + SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date()) + ".jpg"
                when (mode) {
                    "insert" -> {
                        hm.put("mode", "insert")
                        hm.put("id_jadwal", edid_jadwal.text.toString())
                        hm.put("judul", ed_jdl.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                        hm.put("genre", pilihGenre)
                        hm.put("tanggal", ed_tgl.text.toString())
                        hm.put("jam", ed_jam.text.toString())
                    }
                    "update" -> {
                        hm.put("mode", "update")
                        hm.put("id_jadwal", edid_jadwal.text.toString())
                        hm.put("judul", ed_jdl.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                        hm.put("genre", pilihGenre)
                        hm.put("tanggal", ed_tgl.text.toString())
                        hm.put("jam", ed_jam.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode", "delete")
                        hm.put("id_jadwal", edid_jadwal.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaGenre(namaGenre : String) {
        val request = object : StringRequest(
            Request.Method.POST, url2,
            Response.Listener { response ->
                daftarGenre.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarGenre.add(jsonObject.getString("genre"))
                }
                genreAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG)
                    .show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("genre", namaGenre)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    fun showDataJadwal(namaJadwal : String){
        val request = object : StringRequest(
            Request.Method.POST,url4,
            Response.Listener { response ->
                daftarJadwal.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var Jadwal = HashMap<String, String>()
                    Jadwal.put("id_jadwal", jsonObject.getString("id_jadwal"))
                    Jadwal.put("judul", jsonObject.getString("judul"))
                    Jadwal.put("genre", jsonObject.getString("genre"))
                    Jadwal.put("tanggal", jsonObject.getString("tanggal"))
                    Jadwal.put("jam", jsonObject.getString("jam"))
                    Jadwal.put("url", jsonObject.getString("url"))
                    daftarJadwal.add(Jadwal)
                }
                jadwalAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("judul", namaJadwal)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    override fun onStart() {
        super.onStart()
        showDataJadwal("")
        getNamaGenre("")
    }
}

